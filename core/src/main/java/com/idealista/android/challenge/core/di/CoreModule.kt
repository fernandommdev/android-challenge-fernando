package com.idealista.android.challenge.core.di

import com.idealista.android.challenge.core.api.ApiClient
import com.idealista.android.challenge.core.wrench.executor.ChallengeUseCaseExecutor
import com.idealista.android.challenge.core.wrench.executor.UseCaseExecutor
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class CoreModule {
    @Provides
    @Singleton
    fun provideApiClient(): ApiClient = ApiClient.Builder()
            .withEndPoint("https://api.myjson.com/")
            .create()

    @Provides
    @Singleton
    fun provideUseCaseExecutor(): UseCaseExecutor = ChallengeUseCaseExecutor()

}