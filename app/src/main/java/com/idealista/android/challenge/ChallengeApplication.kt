package com.idealista.android.challenge

import android.app.Application
import com.idealista.android.challenge.core.Addressable
import com.idealista.android.challenge.core.CoreAssembler
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import javax.inject.Inject

class ChallengeApplication : Application(), HasAndroidInjector {

    override fun onCreate() {
        super.onCreate()
        Addressable.PACKAGE_NAME = packageName
        CoreAssembler.stringsProvider = StringsProvider(baseContext)
        DaggerAppComponent.builder().application(this).build().inject(this)
    }

    @Inject
    lateinit var androidInjector: DispatchingAndroidInjector<Any>

    override fun androidInjector(): AndroidInjector<Any> = androidInjector

}