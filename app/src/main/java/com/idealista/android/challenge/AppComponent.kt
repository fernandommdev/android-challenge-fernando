package com.idealista.android.challenge

import com.idealista.android.challenge.core.di.CoreModule
import com.idealista.android.challenge.list.di.module.*
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [AndroidSupportInjectionModule::class, AppModule::class, CoreModule::class, ActivityModule::class])
interface AppComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: ChallengeApplication): Builder

        fun build(): AppComponent
    }

    fun inject(myChallengeApplication: ChallengeApplication)
}