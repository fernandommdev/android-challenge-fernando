package com.idealista.android.challenge

import android.content.Context
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule {
    @Provides
    @Singleton
    fun provideContext(application: ChallengeApplication): Context {
        return application.applicationContext
    }
}