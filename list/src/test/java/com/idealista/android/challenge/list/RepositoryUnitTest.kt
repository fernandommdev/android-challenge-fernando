package com.idealista.android.challenge.list

import com.idealista.android.challenge.core.api.ApiClient
import com.idealista.android.challenge.core.api.ListApi
import com.idealista.android.challenge.list.data.ListDataSource
import com.idealista.android.challenge.list.data.ListRepository
import com.idealista.android.challenge.list.domain.Ad
import org.junit.Test

import org.junit.Assert.*

class RepositoryUnitTest {

    private val endpoint: String = "https://api.myjson.com/"

    private val apiClient: ApiClient by lazy {
        ApiClient.Builder()
                .withEndPoint(endpoint)
                .create()
    }

    private val listApi: ListApi by lazy { ListApi(apiClient) }

    /**
     * Method to test if additions to favorites list works correctly.
     */
    @Test
    fun additionToFavoritesList_isCorrect() {
        val listRepository = ListRepository(ListDataSource(listApi))
        var favoritePropertyId: String? = null
        listRepository.list().map {
            favoritePropertyId = it.ads[0].id
            listRepository.markAsFavorite(favoritePropertyId!!)
        }
        var firstElementIdFavoritesList: String? = null
        listRepository.favoritesList().map {
            it.ads[0].id
        }.fold(
                {

                },
                {
                    firstElementIdFavoritesList = it
                }
        )
        assertEquals(favoritePropertyId, firstElementIdFavoritesList)
    }

    /**
     * Method to test if navigation to details works correctly.
     */
    @Test
    fun navigationToDetails_isCorrect() {
        val listRepository = ListRepository(ListDataSource(listApi))
        lateinit var firstElementId: String
        listRepository.list().map {
            firstElementId = it.ads[0].id
        }

        lateinit var ad: Ad
        firstElementId.let {
            listRepository.details(firstElementId).fold({

            }, {
                ad = it
            })
        }

        assertEquals(firstElementId, ad.id)
    }

}
