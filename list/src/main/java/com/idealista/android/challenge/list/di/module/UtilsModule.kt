package com.idealista.android.challenge.list.di.module

import android.content.Context
import com.idealista.android.challenge.core.api.ListApi
import com.idealista.android.challenge.list.data.ListDataSource
import com.idealista.android.challenge.list.utils.ImageLoaderUtils
import com.squareup.picasso.Picasso
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class UtilsModule {
    @Provides
    @Singleton
    fun provideListDataSource(listApi: ListApi): ListDataSource = ListDataSource(listApi)

    @Provides
    @Singleton
    fun provideImageLoaderUtils(context: Context): ImageLoaderUtils = ImageLoaderUtils(Picasso.Builder(context).build())
}