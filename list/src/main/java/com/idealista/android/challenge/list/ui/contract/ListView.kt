package com.idealista.android.challenge.list.ui.contract

import com.idealista.android.challenge.list.ui.ListModel

interface ListView {
    fun render(list: ListModel)
    fun navigateToDetails(propertyCode: String)
    fun navigateToFavoritesList()
}