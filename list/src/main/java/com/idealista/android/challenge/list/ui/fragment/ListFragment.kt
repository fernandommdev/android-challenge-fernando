package com.idealista.android.challenge.list.ui.fragment

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager

import com.idealista.android.challenge.list.R
import com.idealista.android.challenge.list.ui.AdModel
import com.idealista.android.challenge.list.ui.ListModel
import com.idealista.android.challenge.list.ui.adapter.ListAdapter
import com.idealista.android.challenge.list.ui.contract.ListView
import com.idealista.android.challenge.list.ui.presenter.ListPresenter
import com.idealista.android.challenge.list.utils.ImageLoaderUtils
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_list.*
import javax.inject.Inject

class ListFragment : Fragment(), ListView {

    private lateinit var listAdapter: ListAdapter

    @Inject
    lateinit var listPresenter: ListPresenter

    @Inject
    lateinit var imageLoaderUtils: ImageLoaderUtils

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_list, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        activity?.title = getString(R.string.properties)

        listPresenter.view = this
        listAdapter = ListAdapter(imageLoaderUtils)
        recycler.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(activity)
            adapter = listAdapter
        }
        listPresenter.onListNeeded()
    }

    override fun render(list: ListModel) {
        listAdapter.set(list)
        listAdapter.listener(object : ListAdapter.AdListener {
            override fun onAdClicked(ad: AdModel) {
                listPresenter.onAdClicked(ad)
            }
        })
        fab.setOnClickListener {
            listPresenter.onFavoritesButtonTapped()
        }
    }

    override fun navigateToDetails(propertyCode: String) {
        fragmentManager?.beginTransaction()
                ?.replace(R.id.container, DetailsFragment.newInstance(propertyCode))
                ?.addToBackStack(null)
                ?.commit()
    }

    override fun navigateToFavoritesList() {
        fragmentManager?.beginTransaction()
                ?.replace(R.id.container, FavoritesListFragment.newInstance())
                ?.addToBackStack(null)
                ?.commit()
    }

    companion object {
        @JvmStatic
        fun newInstance() = ListFragment()
    }

}
