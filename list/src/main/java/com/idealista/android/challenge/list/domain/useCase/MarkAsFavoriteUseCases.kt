package com.idealista.android.challenge.list.domain.useCase

import com.idealista.android.challenge.core.api.model.CommonError
import com.idealista.android.challenge.core.wrench.type.Either
import com.idealista.android.challenge.list.data.ListRepository

fun markAsFavorite(repository: ListRepository, propertyCode: String): () -> Either<CommonError, Boolean> = {
    repository.markAsFavorite(propertyCode)
}