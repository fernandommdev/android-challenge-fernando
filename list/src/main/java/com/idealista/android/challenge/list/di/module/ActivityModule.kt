package com.idealista.android.challenge.list.di.module

import com.idealista.android.challenge.list.di.scopes.ActivityScope
import com.idealista.android.challenge.list.ui.activity.ListActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module(includes = [UtilsModule::class])
abstract class ActivityModule {
    @ActivityScope
    @ContributesAndroidInjector(modules = [(FragmentModule::class)])
    internal abstract fun contributeListActivity(): ListActivity
}