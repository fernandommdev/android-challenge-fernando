package com.idealista.android.challenge.list.utils

import android.widget.ImageView
import com.squareup.picasso.Picasso

class ImageLoaderUtils(private val picasso: Picasso) {
    fun loadImageOnImageView(imageUrl: String, imageView: ImageView) {
        picasso.load(imageUrl).into(imageView)
    }
}