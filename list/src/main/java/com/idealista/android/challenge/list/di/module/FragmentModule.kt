package com.idealista.android.challenge.list.di.module

import com.idealista.android.challenge.list.di.scopes.FragmentScope
import com.idealista.android.challenge.list.ui.fragment.DetailsFragment
import com.idealista.android.challenge.list.ui.fragment.FavoritesListFragment
import com.idealista.android.challenge.list.ui.fragment.ListFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class FragmentModule {

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeListFragment(): ListFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeDetailsFragment(): DetailsFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeFavoritesListFragment(): FavoritesListFragment

}