package com.idealista.android.challenge.list.ui.presenter

import com.idealista.android.challenge.core.api.model.CommonError
import com.idealista.android.challenge.core.wrench.executor.UseCaseExecutor
import com.idealista.android.challenge.core.wrench.usecase.UseCase
import com.idealista.android.challenge.list.data.ListRepository
import com.idealista.android.challenge.list.domain.List
import com.idealista.android.challenge.list.domain.useCase.list
import com.idealista.android.challenge.list.ui.AdModel
import com.idealista.android.challenge.list.ui.contract.ListView
import com.idealista.android.challenge.list.ui.toModel
import javax.inject.Inject

class ListPresenter @Inject constructor(private val listRepository: ListRepository, private val executor: UseCaseExecutor) {

    lateinit var view: ListView

    fun onListNeeded() {
        UseCase<CommonError, List>()
                .bg(list(listRepository))
                .map { it.toModel() }
                .ui {
                    it.fold(
                            {

                            },
                            {
                                view.render(it)
                            }
                    )
                }.run(executor)
    }

    fun onAdClicked(ad: AdModel) {
        view.navigateToDetails(ad.id)
    }

    fun onFavoritesButtonTapped() {
        view.navigateToFavoritesList()
    }

}