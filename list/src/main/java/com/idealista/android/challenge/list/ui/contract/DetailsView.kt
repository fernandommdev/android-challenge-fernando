package com.idealista.android.challenge.list.ui.contract

import com.idealista.android.challenge.list.ui.AdModel

interface DetailsView {
    fun render(ad: AdModel)
    fun showSuccessMessage()
    fun showErrorMessage()
}