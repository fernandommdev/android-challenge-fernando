package com.idealista.android.challenge.list.ui.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.idealista.android.challenge.list.R
import com.idealista.android.challenge.list.ui.AdModel
import com.idealista.android.challenge.list.ui.contract.DetailsView
import com.idealista.android.challenge.list.ui.presenter.DetailsPresenter
import com.idealista.android.challenge.list.utils.ImageLoaderUtils
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_detail.*
import javax.inject.Inject

class DetailsFragment : Fragment(), DetailsView {

    private val ARG_PROPERTY_CODE = "propertyCode"
    private var propertyCode: String? = null

    @Inject
    lateinit var detailsPresenter: DetailsPresenter

    @Inject
    lateinit var imageLoaderUtils: ImageLoaderUtils

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            propertyCode = it.getString(ARG_PROPERTY_CODE)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_detail, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        activity?.title = getString(R.string.property_details)

        detailsPresenter.view = this
        detailsPresenter.onDetailsNeeded(propertyCode)
    }

    override fun render(ad: AdModel) {
        if (ad.thumbnail.isNotEmpty()) imageLoaderUtils.loadImageOnImageView(ad.thumbnail, ivAd)
        tvTitle.text = ad.title
        tvPrice.text = ad.price
        tvPropertyCode.text = ad.id
        if (ad.favorite) switch1.isChecked = true

        setSwitchListener()
    }

    override fun showSuccessMessage() {
        Toast.makeText(context, getString(R.string.general_favorites_operation_success_message), Toast.LENGTH_SHORT).show()
    }

    override fun showErrorMessage() {
        Toast.makeText(context, getString(R.string.general_error_message), Toast.LENGTH_SHORT).show()
    }

    private fun setSwitchListener() {
        switch1.setOnCheckedChangeListener { _, isChecked ->
            detailsPresenter.onSwitchStateChanged(isChecked)
        }
    }

    companion object {
        @JvmStatic
        fun newInstance(propertyCode: String) =
                DetailsFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PROPERTY_CODE, propertyCode)
                    }
                }
    }

}
