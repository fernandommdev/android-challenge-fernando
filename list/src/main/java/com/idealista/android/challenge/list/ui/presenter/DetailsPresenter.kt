package com.idealista.android.challenge.list.ui.presenter

import com.idealista.android.challenge.core.api.model.CommonError
import com.idealista.android.challenge.core.wrench.executor.UseCaseExecutor
import com.idealista.android.challenge.core.wrench.usecase.UseCase
import com.idealista.android.challenge.list.data.ListRepository
import com.idealista.android.challenge.list.domain.Ad
import com.idealista.android.challenge.list.domain.useCase.details
import com.idealista.android.challenge.list.domain.useCase.markAsFavorite
import com.idealista.android.challenge.list.domain.useCase.unMarkAsFavorite
import com.idealista.android.challenge.list.ui.AdModel
import com.idealista.android.challenge.list.ui.contract.DetailsView
import com.idealista.android.challenge.list.ui.toModel
import javax.inject.Inject

class DetailsPresenter @Inject constructor(private val listRepository: ListRepository, private val executor: UseCaseExecutor) {

    lateinit var view: DetailsView
    private var selectedAd: AdModel? = null

    fun onDetailsNeeded(propertyCode: String?) {
        if (propertyCode != null) {
            UseCase<CommonError, Ad>()
                    .bg(details(listRepository, propertyCode))
                    .map { it.toModel() }
                    .ui {
                        it.fold(
                                {

                                },
                                { ad ->
                                    view.render(ad)
                                    selectedAd = ad
                                }
                        )
                    }.run(executor)
        }
    }

    fun onSwitchStateChanged(checked: Boolean) {
        UseCase<CommonError, Boolean>()
                .bg(if (checked) {
                    markAsFavorite(listRepository, selectedAd?.id ?: "")
                } else {
                    unMarkAsFavorite(listRepository, selectedAd?.id ?: "")
                })
                .ui {
                    it.fold(
                            {
                                //Property already added to favorites
                                view.showErrorMessage()
                            },
                            {
                                //Show correctly added to favorites
                                view.showSuccessMessage()
                            }
                    )
                }.run(executor)
    }

}