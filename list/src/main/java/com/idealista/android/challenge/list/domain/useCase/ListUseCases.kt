package com.idealista.android.challenge.list.domain.useCase

import com.idealista.android.challenge.core.api.model.CommonError
import com.idealista.android.challenge.core.wrench.type.Either
import com.idealista.android.challenge.list.data.ListRepository
import com.idealista.android.challenge.list.domain.List

fun list(repository: ListRepository): () -> Either<CommonError, List> = {
    repository.list()
}