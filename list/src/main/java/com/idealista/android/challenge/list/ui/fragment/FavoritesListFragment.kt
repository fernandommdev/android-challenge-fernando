package com.idealista.android.challenge.list.ui.fragment

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager

import com.idealista.android.challenge.list.R
import com.idealista.android.challenge.list.ui.AdModel
import com.idealista.android.challenge.list.ui.ListModel
import com.idealista.android.challenge.list.ui.adapter.ListAdapter
import com.idealista.android.challenge.list.ui.contract.FavoritesListView
import com.idealista.android.challenge.list.ui.presenter.FavoritesListPresenter
import com.idealista.android.challenge.list.utils.ImageLoaderUtils
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_favorites_list.*
import javax.inject.Inject

class FavoritesListFragment : Fragment(), FavoritesListView {

    private lateinit var listAdapter: ListAdapter

    @Inject
    lateinit var favoritesListPresenter: FavoritesListPresenter

    @Inject
    lateinit var imageLoaderUtils: ImageLoaderUtils

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_favorites_list, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        activity?.title = getString(R.string.favorite_properties)

        favoritesListPresenter.view = this
        listAdapter = ListAdapter(imageLoaderUtils)
        recycler.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(activity)
            adapter = listAdapter
        }
        favoritesListPresenter.onListNeeded()
    }

    override fun render(list: ListModel) {
        listAdapter.set(list)
        listAdapter.listener(object : ListAdapter.AdListener {
            override fun onAdClicked(ad: AdModel) {
                favoritesListPresenter.onAdClicked(ad)
            }
        })
    }

    override fun navigateToDetails(propertyCode: String) {
        fragmentManager?.beginTransaction()
                ?.replace(R.id.container, DetailsFragment.newInstance(propertyCode))
                ?.addToBackStack(null)
                ?.commit()
    }

    companion object {
        @JvmStatic
        fun newInstance() = FavoritesListFragment()
    }

}
