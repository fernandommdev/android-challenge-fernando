package com.idealista.android.challenge.list.data

import com.idealista.android.challenge.core.api.model.CommonError
import com.idealista.android.challenge.core.wrench.type.Either
import com.idealista.android.challenge.list.domain.Ad
import com.idealista.android.challenge.list.domain.List
import com.idealista.android.challenge.list.domain.toDomain
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ListRepository @Inject constructor(private val dataSource: ListDataSource) {

    private val favoritesCodesList = arrayListOf<String>()

    fun list(): Either<CommonError, List> = dataSource.list().map {
        it.toDomain()
    }

    fun favoritesList(): Either<CommonError, List> {
        return dataSource.list().map {
            it.toDomain()
        }.map {
            List(it.ads.filter { ad -> favoritesCodesList.contains(ad.id) })
        }
    }

    fun details(propertyCode: String): Either<CommonError, Ad> = dataSource.list().map {
        it.toDomain().ads.first { ad ->
            ad.id == propertyCode
        }.apply {
            favorite = favoritesCodesList.contains(propertyCode)
        }
    }

    fun markAsFavorite(propertyCode: String): Either<CommonError, Boolean> {
        return if (propertyCode.isEmpty() || favoritesCodesList.contains(propertyCode)) {
            Either.Left(CommonError.Canceled)
        } else {
            Either.Right(favoritesCodesList.add(propertyCode))
        }
    }

    fun unMarkAsFavorite(propertyCode: String): Either<CommonError, Boolean> {
        return if (propertyCode.isEmpty() || !favoritesCodesList.contains(propertyCode)) {
            Either.Left(CommonError.Canceled)
        } else {
            Either.Right(favoritesCodesList.remove(propertyCode))
        }
    }

}